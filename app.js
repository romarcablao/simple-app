const express = require("express")
const path = require('path');;
const router = require("./src");

const app = express();
const port = 8080;

app.get("/version", (req, res) => res.send("Appv1: Hello Stratizens!"));
app.get("/bg", (req, res) => {
  res.sendFile(path.join(__dirname+'/bg.jpg'));
});
app.get("/", (req, res) => {
  res.sendFile(path.join(__dirname+'/welcome.html'));
});
app.use("/", router);

app.listen(port, () => console.log(`App listening on port ${port}!`));
