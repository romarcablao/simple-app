#!/bin/bash
cd simple-app
#Remove Running Containers
docker rm -f $(docker ps -a -q)
#Remove Images
docker image rm -f $(docker image ls -a -q)
#Build Docker Image on EC2
docker image build -t ec2/simple-app .
#Run and Expose to Port 8080
docker run -d -p 80:8080 --name app ec2/simple-app
#List Running Containers
docker ps