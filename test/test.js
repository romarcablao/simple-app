const assert = require("assert");
const { welcomeMessage } = require("../src/operations/welcome");

describe("Unit Test", () => {
  describe("Welcome Message", () => {
    it("should return 'Welcome Stratizens!'", function() {
      assert.equal(welcomeMessage(), "Welcome Stratizens!");
    });
  });
});
