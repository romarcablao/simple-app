# gitlab-ec2-pipeline

Simple gitlab ci/cd - Node App

## Dependencies

1. [NodeJS](https://nodejs.org/en/download/)

2. [Docker](https://docs.docker.com/install/)

## Instructions

1. Clone the repository
   ```bash
   git clone https://gitlab.com/romarcablao/simple-app.git
   cd simple-app
   ```
2. Simply push to this repository and check your update here: http://demo.thecloudspark.com/

---

[Romar Cablao](https://www.linkedin.com/in/romarcablao) | <romarcablao@gmail.com>
